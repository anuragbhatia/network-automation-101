#!/bin/bash

# Script to export specific volume

docker run --rm -v $1:/data -v $(pwd):/dest ubuntu:20.04 tar cvzf /dest/$1.tar /data
