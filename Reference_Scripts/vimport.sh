#!/bin/bash

# Script to import specific volume
# Usage ./vimport volume-name volume-dump.tar


docker run --rm -v $1:/dest -v $(pwd):/source ubuntu:20.04 bash -c "cd /dest && tar xvf /source/$2 --strip 1"